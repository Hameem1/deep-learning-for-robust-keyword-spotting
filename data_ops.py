"""Contains helper functions for data operations."""

import numpy as np
import pandas as pd
from pathlib import Path
from logger import logger
from config import VER, TRAIN_TEST_LABELS
from sklearn.preprocessing import LabelBinarizer


def get_train_test_cases(cases):
    """Creates the DataFrames according to the training/testing 'cases'."""

    from pre_processing import get_dataframe
    data = {}
    for case in cases:
        if len(case) == 1:
            for c in case:
                logger.info(f"\nLoading : _features_{c}")
                data[c] = get_dataframe(VER, df_type=f"_features_{c}")
        elif len(case) > 1:
            logger.info(f"\nConcatenating and Loading :")
            dfs_to_concat = []
            df_name = []
            for c in case:
                logger.info(f"_features_{c} ")
                dfs_to_concat.append(get_dataframe(VER, df_type=f"_features_{c}"))
                df_name.append(c)
            df_name = "_".join(df_name)
            data[df_name] = pd.concat(dfs_to_concat)
        else:
            logger.exception(f"Invalid case : {case}")
    return data


def get_label_binarizer():
    """Returns a label Binarizer object fitted to the TRAIN_TEST_LABELS."""

    lb = LabelBinarizer()
    lb.fit(TRAIN_TEST_LABELS)
    return lb


def get_model_type(model_path, quantized=False):
    """
    Takes in the model filename or path and returns the model format and model type.

    Parameters
    ----------
    model_path : str or Path
        The filename or Path of the model file.
    quantized : bool
        If True, returns quantization type as well.

    Returns
    -------
    model_type : str
        Model type. 'cnn', 'dscnn', 'dnn', etc.
    model_format : str
        Can be 'keras', 'tf-lite' or 'quantized'.
    quantization_type : str
        Can be '8-bit', 'float-16' or 'full-int'

    """
    if isinstance(model_path, (Path, str)):
        stem = Path(model_path).stem
    else:
        logger.exception(f'invalid input type : {type(model_path)}')

    if stem.__contains__("tflite"):
        model_format = 'tf-lite'
        stem = stem.replace("tflite_", "")
        if stem.__contains__("quantized"):
            model_format = 'quantized'
            stem = stem.replace("_quantized", "")
    else:
        model_format = 'keras'

    model_type = stem.split("_")[0]
    model_type = model_type.split("-")[0].lower()

    if quantized:
        quantization_type = stem.split("_")[-1]
        return model_type, model_format, quantization_type
    else:
        return model_type, model_format


def model_name_parser(x):
    y = np.array(x.split("_"))
    if y.shape[0] < 5:
        y = np.insert(y, 3, " ")
    y = y[:-1]
    return tuple(y)



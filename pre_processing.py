"""Contains functionality for the data pre-processing phase of the pipeline."""

import os
import re
from shutil import copyfile
import hashlib
import librosa
import numpy as np
import pandas as pd
from tqdm import tqdm
from pathlib import Path
from scipy.io import wavfile
from pyroomacoustics.datasets.utils import download_uncompress
from data_structure import AudioFile
from logger import logger
from config import DATA_PATH_v1, DATA_PATH_v2, URL_DATASET_V1, URL_DATASET_V2, MODELS_DIR, LOGS_DIR, \
    DATA_FILES_DIR, DATASET_DIR, DATASET_PROCESSED_DIR, DATA_PATH_PROCESSED_v1, DATA_PATH_PROCESSED_v2, \
    DATA_AUGMENTATION, DATAFRAMES_DIR, TEST_PERCENT, VALIDATE_PERCENT, VER, TRAIN_TEST_LABELS, AUGMENTS, \
    AUGMENTED_DIR, GENERATE_FEATURES


# TODO: Clean the main datasets to remove silent tracks. Use enveloping to find the signal levels.
# TODO: Make sure that the randomly selected sample is actually noisy.


def verify_dir_struct():
    """
    Verifies the project's directory structure.
    """

    # List of all the directories to verify
    dir_list = [LOGS_DIR, MODELS_DIR, DATA_FILES_DIR, DATASET_DIR, DATAFRAMES_DIR,
                DATASET_PROCESSED_DIR, DATA_PATH_PROCESSED_v1, DATA_PATH_PROCESSED_v2]
    # Creating any missing directories from dir_list
    try:
        logger.info(f'Verifying Directory Structure')
        for d in dir_list:
            if not d.exists():
                logger.warning(f'Directory not found in project root : "{d}"')
                os.mkdir(d)
                logger.info(f'Created directory : "{d}"')
    except Exception as e:
        logger.exception(f'Error in func "{verify_dir_struct.__name__}" : {e}')
    else:
        logger.info(f'Directory structure verified!\n')


def clean_directory(dir_path, file_ext=None, prompt=True, verbose=False):
    """
    Cleans a directory by removing all files with the given file extension.

    Parameters
    ----------
    dir_path : str or Path
        The directory path to recursively clean.
    file_ext : str
        The file extension of the files to be removed. If None, displays all files/filepaths.
    prompt: bool
        If True, displays a prompt before removing the files.
    verbose: If True, displays info for every step of the directory walk.

    """

    total_files = list()
    filepaths = list()
    files_to_remove = list()
    filepaths_to_remove = list()

    for root, dirs, files in os.walk(dir_path):
        if verbose:
            logger.info(f'\nDirectory : {root}\nSub-directories : {dirs}\nFiles : {files}\nNo. of Files = {len(files)}')
        if file_ext:
            files_to_remove.extend([(f) for f in files if f.endswith(file_ext)])
            filepaths_to_remove.extend([(Path(root) / f) for f in files if f.endswith(file_ext)])
        total_files.extend(files)
        filepaths.extend([Path(root) / f for f in files])

    if file_ext is None:
        logger.info("------------")
        logger.info(f"Total Files = {len(total_files)}\n{total_files}")
        logger.info("------------")
        logger.info(f"Total Paths = {len(filepaths)}\n{filepaths}")
        logger.info("------------")
    else:
        logger.info("------------")
        logger.info(f"Total Files to remove = {len(files_to_remove)}\n{files_to_remove}")
        logger.info("------------")
        logger.info(f"Total Paths to remove = {len(filepaths_to_remove)}\n{filepaths_to_remove}")
        logger.info("------------")

    if prompt:
        res = str(input(f"Would you like to remove these {len(files_to_remove)} files? (y/n)")).lower()
    else:
        res = 'y'

    if res == 'y':
        for fp in tqdm(filepaths_to_remove, f"Deleting '{file_ext}' files in {dir_path}."):
            os.unlink(fp)
    else:
        logger.info("Operation cancelled.\n")


def get_datasets(ver_1=True, ver_2=True):
    """
    Downloads and un-compresses both version 1 and 2 of the dataset into DATASET_DIR.

    Parameters
    ----------
    ver_1 : bool
        Downloads speech_commands_v0.01 if True
    ver_2 : bool
        Downloads speech_commands_v0.02 if True

    """

    if ver_1:
        if not DATA_PATH_v1.exists():
            download_uncompress(url=URL_DATASET_V1, path=DATA_PATH_v1)
        else:
            logger.info(f"Found dataset_v1 in project directory: '{DATA_PATH_v1}'")
            logger.info(f"Using '{DATA_PATH_v1}'")

    if ver_2:
        if not DATA_PATH_v2.exists():
            download_uncompress(url=URL_DATASET_V2, path=DATA_PATH_v2)
        else:
            logger.info(f"Found dataset_v2 in project directory: '{DATA_PATH_v2}'")
            logger.info(f"Using '{DATA_PATH_v2}'")


# FIXME : should only work for a single dataset (currently redundant)
def dataset_to_df(dataset_path, filename_append="", calc_runtime=True, make_csv=False):
    """
    Converts the given dataset to a df with columns ('file_path', 'label', 'runtime' [optional]).

    Saves the DataFrame(s) in DATAFRAMES_DIR (default).

    Parameters
    ----------
    dataset_path : str or Path or list(strs/Paths)
        Path to the dataset(s).
    filename_append : str
        Optional appendage to the .csv file name (e.g., "_clean")
    calc_runtime : bool
        Returned DataFrame includes a 'runtime' column if True.
    make_csv : bool
        Creates a .csv file from the DataFrame as well.

    Returns
    -------
    df : DataFrame or tuple(DataFrames)
        Pandas DataFrame(s) with columns ('file_path', 'label', 'runtime' [optional])

    """

    # Initializing empty lists of datasets (i/p) and DataFrames (o/p)
    datasets = []
    dfs = []
    skipped = []

    # Checking if dataset_path contains a single (str/Path) or multiple entries (list)
    if isinstance(dataset_path, (Path, str)):
        datasets = [dataset_path]
    elif isinstance(dataset_path, list):
        datasets = list(dataset_path)

    # For the given dataset(s)
    for dataset_path in datasets:
        file_name = Path(dataset_path).name + filename_append + ".csv"
        if make_csv:
            # if the corresponding .csv file already exists
            if (DATAFRAMES_DIR / file_name).exists():
                res = str(input(f'Overwrite existing file "{file_name}"? (y/n)\n')).lower()
                while res not in ['y', 'n']:
                    res = str(input(f'Overwrite existing file "{file_name}"? (y/n)\n')).lower()
                if res == 'y':
                    logger.info(f'Overwriting file "{file_name}"')
                    skipped.append(False)
                if res == 'n':
                    logger.info(f'Skipping file "{file_name}"')
                    dfs.append(pd.read_csv(DATAFRAMES_DIR / f"{file_name}", sep='\t', index_col=0))
                    skipped.append(True)
                    continue
            else:
                skipped.append(False)

        # Getting the available audio classes from the dataset
        audio_classes = [audio_class for audio_class in os.listdir(dataset_path)
                         if not audio_class.__contains__('.')
                         and audio_class.islower()]

        # Storing the files and their labels in a dictionary
        data_dict = dict(file_path=[], label=[])
        for audio_class in audio_classes:
            files = [Path(dataset_path / audio_class / f).as_posix() for f in os.listdir(dataset_path / audio_class)
                     if f.endswith(".wav")]
            labels = [audio_class] * len(files)
            data_dict['file_path'] += files
            data_dict['label'] += labels

        # Creating a DataFrame from the data_dict and adding a "runtime" attribute (in secs)
        df = pd.DataFrame(data_dict)
        df.set_index('file_path', inplace=True)
        if calc_runtime:
            for file in tqdm(df.index, f"Calculating the runtime attribute."):
                fs, signal = wavfile.read(file)
                df.at[file, 'runtime'] = signal.shape[0] / fs
        dfs.append(df)

    if make_csv:
        # Saving the DataFrame(s) as .csv files
        for ds, df, skip in zip(datasets, dfs, skipped):
            # if user skipped the DataFrame
            if skip:
                continue
            df.to_csv(DATAFRAMES_DIR / file_name, sep='\t')
            logger.info(f'DataFrame saved : "{DATAFRAMES_DIR / file_name}"')

    # Making sure the number of returned DataFrames is equal to the number of datasets provided
    assert (len(dfs) == len(datasets)), \
        f"The length of 'DataFrames' ({len(dfs)}) is not equal to 'datasets' ({len(datasets)}) "

    # Returning either a DataFrame or Tuple(DataFrames)
    if len(dfs) == 1:
        return dfs[0]
    else:
        return tuple(dfs)


def get_dataframe(dataset_version, df_type=""):
    """
    Returns the DataFrame for the given dataset version.

    Parameters
    ----------
    dataset_version : str
        Version of the dataset, e.g. "v1" or "v2"
    df_type : str
        Can be '' (default), '_clean', '_features_clean', etc.
    """

    _error_msg = f"The DataFrame for dataset '{dataset_version}{df_type}' does not exist!\n" \
                 f"Please create the DataFrame first by running 'dataset_to_df'.\n"

    _ver = dataset_version
    if _ver == 'v1' or _ver == 'v2':
        _filename = f"{DATA_PATH_v1.name}{df_type}.csv" if _ver == 'v1' else f"{DATA_PATH_v2.name}{df_type}.csv"
        _directory = DATAFRAMES_DIR / 'features' if df_type.__contains__('_features') else DATAFRAMES_DIR
        if Path(_directory / _filename).exists():
            return pd.read_csv(_directory / _filename, sep='\t', index_col=0)
        else:
            raise Exception(_error_msg)
    else:
        logger.error(f"Invalid dataset_version '{_ver}'!")


def samples_to_df(df, sample_len, n_samples, fs):
    """
    Converts the long running audio files - in 'df' - in to 'n_samples'
    of length 'sample_len' and returns them in a DataFrame.

    Parameters
    ----------
    df : pandas.DataFrame
        A DataFrame only containing audio files with label '_background_noise_'.
    sample_len : int
        Sample size (in seconds).
    n_samples : int
        The number of samples to construct.
    fs : int
        The sampling frequency of the provided audio files.

    Returns
    -------
    df : pandas.DataFrame
        A DataFrame containing _background_noise_ as samples of length 'sample_len'.

    """

    # Sample length in seconds
    sample_len = int(sample_len * fs)  # 1 second sample

    # Data containers for the Samples
    rand_samples = []
    sample_file_paths = []
    sample_labels = []

    # Populating the sample data containers
    for _ in range(n_samples):
        # Pick a random file
        rand_file = df.sample(1)
        # Get the complete signal for the file
        rand_signal = rand_file['signal'].values[0]
        # Get a random index to start the sample from
        rand_pos = np.random.randint(len(rand_signal) - sample_len)
        # Get a random sample of size sample_len
        rand_sample = rand_signal[rand_pos:rand_pos + sample_len]
        # Append the sample and it's related info to the data containers
        rand_samples.append(rand_sample)
        sample_file_paths.append(rand_file.index[0])
        sample_labels.append(rand_file['label'][0])

    # Making the file names unique for each noise sample
    sample_file_paths = [sfp.replace(".wav", f"_{i}.wav") for i, sfp in enumerate(sample_file_paths)]
    # Calculating the runtime for all the samples
    sample_runtime = [sig.shape[0] / fs for sig in rand_samples]
    # Creating a dict suitable for conversion to DataFrame
    dic = dict(file_path=sample_file_paths, label=sample_labels, runtime=sample_runtime, signal=rand_samples)
    # Creating a DataFrame for the samples (same structure as the main dataframe + 'signals')
    df_samples = pd.DataFrame(dic)
    # Setting the 'file_path' column as the index
    df_samples.set_index('file_path', inplace=True)
    return df_samples


def get_silence_df(df, n_samples, sample_len=1):
    """
    Returns a DataFrame containing samples of label 'silence'.

    Parameters
    ----------
    df : pandas.DataFrame
        The main DataFrame.
    sample_len : int
        Sample size (in seconds).
    n_samples : int
        The number of samples to construct.

    Returns
    -------
    df : pandas.DataFrame
        Columns(file_path, label, runtime, signal)

    """

    # Getting a noise only dataframe
    df_noise = pd.DataFrame(df[df['label'] == '_background_noise_'])
    # Adding a 'signal' column and setting it to NaN
    df_noise['signal'] = np.nan
    # Changing the column type to 'object' to allow storing np.arrays/lists
    df_noise['signal'] = df_noise['signal'].astype(object)
    # Populating the 'signal' column
    signal_iterator = (AudioFile(i).signal for i in df_noise.index)
    for i, s in zip(df_noise.index, signal_iterator):
        df_noise.at[i, 'signal'] = s
    # Getting the sampling frequency
    fs = AudioFile(df_noise.index[0]).fs
    # Generating random samples (as a DataFrame) from noise audio to use as data for 'silence'.
    df_silence = samples_to_df(df_noise, sample_len, n_samples, fs)
    # Changing the labels : "_background_noise_"
    df_silence['label'] = ['silence'] * len(df_silence)
    return df_silence


def process_dataframe(dataset_version, original_df, silence_df, test_percentage, validation_percentage=0,
                      force_generate=False):
    """
    Cleans up the given DataFrame.

    Saves the updated DataFrame by appending "_clean" to it.

    This DataFrame describes the new dataset in dataset_processed.

    Updates the audio labels to match with TRAIN_TEST_LABELS.

    Calculates a 'category' label which assigns each file to a train/test/validate set.

    Parameters
    ----------
    dataset_version : str
        The dataset version for the provided DataFrames.
    original_df : pandas.DataFrame
        The main DataFrame to be processed.
    silence_df : pandas.DataFrame
        The DataFrame containing samples for the label 'silence'.
    test_percentage :
        The percentage value of testing samples (e.g., 30).
    validation_percentage : int or float
        The percentage value of validation samples (e.g., 10).
    force_generate : bool
        To overwrite existing dataset directories (processed).

    Returns
    -------
    df : pandas.DataFrame
        The processed DataFrame (file_path, label, runtime, category).

    """
    # Checking dataset version (for saving purposes)
    if dataset_version == "v1":
        target_df_path = DATAFRAMES_DIR / f"{DATA_PATH_v1.name}_clean.csv"
        target_dataset_path = DATA_PATH_PROCESSED_v1
    elif dataset_version == "v2":
        target_df_path = DATAFRAMES_DIR / f"{DATA_PATH_v2.name}_clean.csv"
        target_dataset_path = DATA_PATH_PROCESSED_v2
    else:
        logger.error(f"Invalid dataset_version '{dataset_version}'!")

    # Creating .wav files for the 'silence' samples
    silence_dir = target_dataset_path / 'silence'
    if not silence_dir.exists() or force_generate:
        silence_dir.mkdir()
        # Getting the sampling frequency
        fs = AudioFile(original_df.index[0]).fs
        for f in tqdm(silence_df.index, f"Generating .wav files for label : 'silence'"):
            filename = Path(f).name
            # Writing out the .wav file
            librosa.output.write_wav(silence_dir / filename, silence_df.loc[f, 'signal'], fs)
    else:
        logger.warning(f"Path already exists : {silence_dir}\n")

    # Removing the _background_noise_ files
    modified_df = pd.DataFrame(original_df[original_df['label'] != '_background_noise_'])
    # Changing any label which is not in TRAIN_TEST_LABELS to "unknown_word"
    modified_df.loc[~modified_df['label'].isin(TRAIN_TEST_LABELS), 'label'] = 'unknown_word'

    # Further cleanup should be added here [to the modified_df] #
    # Excluding audio files of runtime lower than 1.0
    del_filter = (modified_df['runtime'] == 1.0)
    modified_df = modified_df[del_filter]

    # Creating the new dataset (processed)
    for label in modified_df['label'].unique():
        folder_path = target_dataset_path / label

        if (not folder_path.exists() or force_generate) and (label != 'unknown_word'):
            folder_path.mkdir()
            files = modified_df[modified_df['label'] == label].index
            for f in tqdm(files, f"Copying .wav files for label : '{label}'"):
                filename = Path(f).name
                copyfile(f, (folder_path / filename).as_posix())

        elif (not folder_path.exists() or force_generate) and (label == 'unknown_word'):
            if not folder_path.exists():
                folder_path.mkdir()
            files = modified_df[modified_df['label'] == label].index
            for i, f in tqdm(enumerate(files), f"Copying and renaming .wav files for label : '{label}'"):
                filename = Path(f).name
                new_filename = filename.replace('_nohash_', f"_no_hash_{i}_")
                copyfile(f, (folder_path / new_filename).as_posix())

        else:
            logger.warning(f"Path already exists : {folder_path}\n")

    del silence_df, modified_df

    # Reading in the newly generated dataset
    processed_df = dataset_to_df(target_dataset_path, '_clean')
    # Getting the train/test/validate categories for each DataFrame sample
    processed_df = get_train_test_categories(processed_df, test_percentage, validation_percentage)
    # Saving the df_processed (_clean) as .csv
    processed_df.to_csv(target_df_path, sep='\t')

    return processed_df


def get_random_noise_sample(noise_source_path, sample_size):
    """
    Returns a random noise sample for the given sample_size, from the given noise_source directory path.

    Parameters
    ----------
    noise_source_path : str or Path
        The source path for the noise files.
    sample_size : int
        The number of sample points to include in the returned sample.

    Returns
    -------
    noise_sample : numpy.ndarray
        A randomly selected sample of noise data from the given dataset.

    """

    # Get all the available .wav files
    files = [Path(file.path) for file in os.scandir(noise_source_path) if file.is_file() and file.name.endswith(".wav")]
    if len(files) == 6:
        # Choose a random noise file (with prob distribution)
        # suppresses selection of white and pink noise.
        noise_file = np.random.choice(files, p=[0.2, 0.2, 0.2, 0.1, 0.2, 0.1])
    else:
        # Choose a random noise file
        noise_file = np.random.choice(files)
    # Get the signal and fs for the noise file
    noise_signal, fs = librosa.load(noise_file, sr=None, mono=True)
    # Select a random point for the start of the sample
    random_point = np.random.randint(noise_signal.size - sample_size)
    # Get a noise sample of size sample_size, starting at the random_point
    noise_sample = noise_signal[random_point: random_point + sample_size]
    return noise_sample


def augment_data(df, dataset_version, test_percentage, validation_percentage=0, transform=None, time_stretch=(1.0, 1.0),
                 freq_shift=(0.0, 0.0), noise_ratio=(0.0, 0.0), signal_ratio=1.0, noise_path=None, filename_append="",
                 make_csv=False):
    """
    Generates an augmented dataset (in time, freq or noise level) from the source dataset given by 'dateset_version'.

    Parameters
    ----------
    df : pandas.DataFrame
        A DataFrame containing the 'file_path' as Index and the associated 'label'. Other columns can also exist.
    dataset_version : str
        The version of the dataset to augment "v1" or "v2".
    test_percentage :
        The percentage value of testing samples (e.g., 30).
    validation_percentage : int or float
        The percentage value of validation samples (e.g., 10).
    transform : str
        The transformation to apply "time", "freq" or "noise".
    time_stretch : float
        The factor by which the audio signal should be stretched/compressed in time.
    freq_shift : float
        The number of semitones to shift the pitch by.
    noise_ratio : float
        The ratio of noise (0.0 - 1.0) to add to the signal.
    signal_ratio : float
        The ratio of the original signal (0.0 - 1.0). Default : 1.0.
    noise_path : Path or str
        Path to an external directory containing noise files to use for augmentation.
    filename_append : str
        Optional appendage to the .csv file name (e.g., "_clean")
    make_csv : bool
        Creates a .csv file from the DataFrame as well.

    Returns
    -------
    df_augmented : pandas.DataFrame
        The augmented DataFrame (file_path, label, runtime, category).

    """

    if dataset_version == "v1":
        target_path = DATA_PATH_PROCESSED_v1 / AUGMENTED_DIR
        noise_source_path = DATA_PATH_v1 / "_background_noise_"
        filename = f"{DATA_PATH_v1.name}{filename_append}.csv"
    elif dataset_version == "v2":
        target_path = DATA_PATH_PROCESSED_v2 / AUGMENTED_DIR
        noise_source_path = DATA_PATH_v2 / "_background_noise_"
        filename = f"{DATA_PATH_v2.name}{filename_append}.csv"
    else:
        logger.error(f"Invalid dataset_version '{dataset_version}'!")

    # Creating the sub-directory for the augmented data and performing transform specific configuration
    if transform == 'time':
        target_path = target_path / f'{transform}'
    elif transform == 'freq':
        target_path = target_path / f'{transform}'
        freq_range = list(range(freq_shift[0], freq_shift[1] + 1))
        freq_range.remove(0)
    elif transform == 'noise':
        target_path = target_path / f'{transform}'
    elif transform == 'heavy_noise':
        target_path = target_path / f'{transform}'
        if noise_path is not None:
            noise_source_path = noise_path
    else:
        logger.error(f"Invalid transform '{transform}'!")

    # Checking if the augmented DataFrame already exists
    if not (DATAFRAMES_DIR / filename).exists():
        # Checking if the augmented dataset already exists
        if not target_path.exists():
            target_path.mkdir(parents=True)
            logger.info(f'\nGenerating the augmented dataset : {filename_append}.\n')
            for _file in df.index:
                new_dir_path = target_path / str(df.loc[_file, 'label'])
                file_path = Path(_file)
                if not new_dir_path.exists():
                    logger.warning(f'Directory not found : "{new_dir_path}"')
                    new_dir_path.mkdir()
                    logger.info(f'Created directory : "{new_dir_path}"')
                signal, fs = librosa.load(file_path.as_posix(), sr=None, mono=True)
                # applying the respective transform
                if transform == 'time':
                    stretch = round(np.random.uniform(time_stretch[0], time_stretch[1]), 2)
                    modified_signal = librosa.effects.time_stretch(signal, stretch)
                if transform == 'freq':
                    shift = np.random.choice(freq_range)
                    modified_signal = librosa.effects.pitch_shift(signal, fs, n_steps=shift)
                if transform == 'noise' or transform == 'heavy_noise':
                    noise_signal = get_random_noise_sample(noise_source_path, len(signal))
                    noise_factor = round(np.random.uniform(noise_ratio[0], noise_ratio[1]), 2)
                    modified_signal = (signal * signal_ratio) + (noise_signal * float(noise_factor))
                new_file_path = f"{new_dir_path.stem}/{file_path.stem + f'_{transform}_{str(noise_ratio)}' + '.wav'}"
                # creating the augmented audio file
                librosa.output.write_wav(target_path / new_file_path, modified_signal, fs)

        else:
            logger.warning(f"The augmented dataset for '{transform}' already exists!\n")

        # creating a DataFrame from the new augmented dataset
        df_augmented = dataset_to_df(target_path, calc_runtime=True)
        # getting the 'categories' column for the DataFrame
        df_augmented = get_train_test_categories(df_augmented, test_percentage, validation_percentage)
        if make_csv:
            df_augmented.to_csv(DATAFRAMES_DIR / filename, sep='\t')
        return df_augmented

    else:
        logger.info(f"Augmented DataFrame already exists : {filename}")
        logger.info(f"Loading the processed DataFrame from:{DATAFRAMES_DIR / filename}\n")
        return get_dataframe(dataset_version, df_type=filename_append)


def which_set(filename, validation_percentage, testing_percentage):
    MAX_NUM_WAVS_PER_CLASS = 2 ** 27 - 1  # ~134M

    """
    THIS FUNCTION HAS BEEN TAKEN AS IS FROM THE README FILE OF THE OPEN SOURCE GOOGLE SPEECH COMMANDS DATASET.
    
    Determines which data partition the file should belong to. 

    We want to keep files in the same training, validation, or testing sets even
    if new ones are added over time. This makes it less likely that testing
    samples will accidentally be reused in training when long runs are restarted
    for example. To keep this stability, a hash of the filename is taken and used
    to determine which set it should belong to. This determination only depends on
    the name and the set proportions, so it won't change as other files are added.

    It's also useful to associate particular files as related (for example words
    spoken by the same person), so anything after '_nohash_' in a filename is
    ignored for set determination. This ensures that 'bobby_nohash_0.wav' and
    'bobby_nohash_1.wav' are always in the same set, for example.

    Args:
    filename: File path of the data sample.
    validation_percentage: How much of the data set to use for validation.
    testing_percentage: How much of the data set to use for testing.

    Returns:
    String, one of 'training', 'validation', or 'testing'.
    """
    base_name = os.path.basename(filename)
    # We want to ignore anything after '_nohash_' in the file name when
    # deciding which set to put a wav in, so the data set creator has a way of
    # grouping wavs that are close variations of each other.
    hash_name = re.sub(r'_nohash_.*$', '', base_name)
    # This looks a bit magical, but we need to decide whether this file should
    # go into the training, testing, or validation sets, and we want to keep
    # existing files in the same set even if more files are subsequently
    # added.
    # To do that, we need a stable way of deciding based on just the file name
    # itself, so we do a hash of that and then use that to generate a
    # probability value that we use to assign it.
    hash_name_hashed = hashlib.sha1(hash_name.encode('utf-8')).hexdigest()
    percentage_hash = ((int(hash_name_hashed, 16) %
                        (MAX_NUM_WAVS_PER_CLASS + 1)) *
                       (100.0 / MAX_NUM_WAVS_PER_CLASS))
    if percentage_hash < validation_percentage:
        result = 'validation'
    elif percentage_hash < (testing_percentage + validation_percentage):
        result = 'testing'
    else:
        result = 'training'
    return result


def get_train_test_categories(df, test_percentage, validation_percentage=0):
    """
    Generates a 'category' column for the given DataFrame (df). Treats silence and non-silence samples differently.

    Parameters
    ----------
    df : pandas.DataFrame
        A DataFrame containing the 'file_path' as Index and the associated 'label'. Other columns can also exist.
    test_percentage :
        The percentage value of testing samples (e.g., 30).
    validation_percentage : int or float
        The percentage value of validation samples (e.g., 10).

    Returns
    -------
    df : pandas.DataFrame
        The updated DataFrame with the 'category' column added.

    """
    non_silence_categories = []
    silence_categories = []

    non_silence_filter = df['label'] != 'silence'
    silence_filter = df['label'] == 'silence'

    df['category'] = np.nan
    # Getting 'category' column for both 'silence' and 'non-silence' labels
    non_silence_files = df[non_silence_filter].index
    if len(non_silence_files) > 0:
        for f in non_silence_files:
            non_silence_categories.append(which_set(f, validation_percentage, test_percentage))

        assert len(non_silence_categories) == len(df[non_silence_filter].index), \
            f"'non_silence_categories' ({len(non_silence_categories)}) not the same length as " \
            f"'df[non_silence_filter]' ({len(df[non_silence_filter].index)})!!"
        df.loc[non_silence_filter, 'category'] = non_silence_categories

    silence_files = df[silence_filter].index
    if len(silence_files) > 0:
        num_files = len(silence_files)
        num_test = int(num_files * test_percentage * 0.01)
        num_validate = int(num_files * validation_percentage * 0.01)
        category_count = dict(testing=num_test,
                              training=num_files - (num_test + num_validate),
                              validation=num_validate)
        for k, v in category_count.items():
            silence_categories.extend([k] * v)
        np.random.shuffle(silence_categories)

        assert (len(silence_categories) == len(df[silence_filter].index)), \
            f"'silence_categories' ({len(silence_categories)}) not the same length as" \
            f"'df[silence_filter]' ({len(df[silence_filter].index)})!!"
        df.loc[silence_filter, 'category'] = silence_categories

    return df


def generate_features(df, dataset_version, filename_append="", make_csv=False):
    """
    Generates the feature files (.npy) and returns the features DataFrame for the given DataFrame.

    Parameters
    ----------
    df : pandas.DataFrame
        A DataFrame containing the 'file_path' as Index along with 'label', 'runtime' and 'category'.
    dataset_version : str
        The version of the dataset to augment "v1" or "v2".
    filename_append : str
        Optional appendage to the .csv file name (e.g., "_clean")
    make_csv : bool
        Creates a .csv file from the DataFrame as well.

    Returns
    -------
    df_features : pandas.DataFrame
        The features DataFrame (file_path, label, shape, category).
    """

    filename = f"{DATA_PATH_v1.name}{filename_append}.csv" \
        if dataset_version == 'v1' else f"{DATA_PATH_v2.name}{filename_append}.csv"
    _features_dir = DATAFRAMES_DIR / 'features'

    # Checking if the .csv file already exists
    if not (_features_dir / filename).exists():
        # Holds the updated index for the features DataFrame
        new_index = []
        # count of newly created files (.npy)
        _new_count = 0
        # Holds the 'shape' information for each feature
        new_shape = []
        # A generator of AudioFile objects for the files in df
        audio_objs = (AudioFile(audio_file) for audio_file in df.index)
        # Calculating the mfcc for the given .wav files and Saving them as .npy files in the same directory
        logger.info(f'\nGenerating Features as (.npy) files : {filename_append}.\n')
        for audio_obj in audio_objs:
            audio_obj.calc_mfcc()
            new_filepath = Path(str(audio_obj.filepath).replace('.wav', '.npy'))
            if not new_filepath.parents[0].exists():
                new_filepath.mkdir(parents=True)
                logger.info(f'Created directory : "{new_filepath.parents[0]}"')
            # Create the features file if it doesn't already exist
            if not new_filepath.exists():
                np.save(new_filepath, audio_obj.mfcc)
                _new_count += 1
            # Constructing new_index
            new_index.append(new_filepath.as_posix())
            new_shape.append(str(audio_obj.mfcc.shape))

        # Creating the features DataFrame and saving it
        # Updating the index
        df_features = pd.DataFrame(df)
        df_features.set_index(pd.Index(new_index), inplace=True)
        df_features.index.rename('file_path', inplace=True)
        # Replacing 'runtime' with a new 'shape' column, as it is more appropriate for feature data.
        df_features.rename(columns={'runtime': 'shape'}, inplace=True)
        df_features['shape'] = new_shape
        # Saving as .csv file
        if make_csv:
            _features_dir = DATAFRAMES_DIR / 'features'
            df_features.to_csv(_features_dir / filename, sep='\t')
        # logging info
        logger.info(f"For DataFrame '{dataset_version}{filename_append}'")
        logger.info(f"Generated features (.npy) for {len(df.index)} files. "
                    f"{_new_count} new, {len(df.index) - _new_count} existing.\n")
        return df_features
    else:
        logger.info(f"Features DataFrame already exists : {filename}")
        logger.info(f"Loading the processed DataFrame from:{_features_dir / filename}\n")
        return get_dataframe(dataset_version, df_type=filename_append)


if __name__ == '__main__':

    # --------------
    # Pre-processing (Data cleaning, Data Structuring, Data Augmentation, Data Generation)
    # --------------

    # Skipping initialization if the processed (clean) DataFrame already exists.
    df_processed_name = f"{DATA_PATH_v1.name}_clean.csv" if VER == 'v1' else f"{DATA_PATH_v2.name}_clean.csv"
    if (DATAFRAMES_DIR / df_processed_name).exists():
        logger.info(f"Loaded the processed DataFrame from:\n{DATAFRAMES_DIR / df_processed_name}")
        df_processed = get_dataframe(VER, df_type="_clean")
    else:
        # Verifying the project's directory structure
        verify_dir_struct()
        # Obtaining the datasets and converting to DataFrames
        get_datasets()
        # Getting the DataFrame for dataset v2
        df_main = get_dataframe(VER)
        logger.info(f"Dataset '{VER}' loaded in as a DataFrame -> 'df_main'.")
        logger.info(df_main.describe())
        # Getting the word labels for the raw DataFrame
        audio_classes_main = list(df_main['label'].unique())
        logger.info(f"Audio classes for the raw dataset '{VER}':\n{audio_classes_main}\n")
        # A dataframe of 'silence' data (30k samples)
        df_silence = get_silence_df(df_main, n_samples=int(3e4), sample_len=1)
        # Getting the processed dataframe containing the labels to train/test/validate with
        df_processed = process_dataframe(VER, df_main, df_silence, TEST_PERCENT, VALIDATE_PERCENT)

    # Getting the word labels for the clean DataFrame
    audio_classes_processed = list(df_processed['label'].unique())
    logger.info(f"Audio classes for the processed dataset '{VER}':\n{audio_classes_processed}\n")

    # If Data Augmentation is enabled
    # contains a dict of keys which are the augment names (time, freq, noise, heavy_noise)
    augmented_dfs = {}
    if DATA_AUGMENTATION:
        logger.info(f'Generating dataset augments for dataset {VER}\n')
        # Creating multiple augmented datasets from the processed dataset
        for augment in AUGMENTS:
            _transform_name = list(augment.values())[0]
            augmented_dfs[_transform_name] = augment_data(df_processed, VER, TEST_PERCENT, VALIDATE_PERCENT,
                                                          filename_append=f"_augmented_{_transform_name}",
                                                          make_csv=True, **augment)

    # Collecting DataFrames to generate features for
    gen_features = dict({'clean': df_processed}, **augmented_dfs)
    del augmented_dfs

    feature_dfs = {}
    # If Feature Generation is enabled
    if GENERATE_FEATURES:
        for df_name, df in gen_features.items():
            feature_dfs[df_name] = generate_features(df, dataset_version=VER, filename_append=f"_features_{df_name}",
                                                     make_csv=True)

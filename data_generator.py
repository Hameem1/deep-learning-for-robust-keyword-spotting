"""Implements on the fly data generation for training and testing models."""

import numpy as np
from tensorflow.keras.utils import Sequence
from sklearn.preprocessing import LabelBinarizer
from config import VER, TRAIN_TEST_LABELS
from pre_processing import get_dataframe


# TODO: Add documentation
class AudioFeed(Sequence):
    def __init__(self, df, model_type, batch_size=64, dim=(49, 20), shuffle=True, num_channels=1):
        self.df = df
        self.model_type = model_type
        self.dim = dim
        self.batch_size = batch_size
        self.samples = list(self.df.index)
        self.indexes = None     # Updated in on_epoch_end()
        self.shuffle = shuffle
        self.num_channels = num_channels
        self.on_epoch_end()
        self.lb = LabelBinarizer()
        self.lb.fit(TRAIN_TEST_LABELS)

    def __len__(self):
        return self.df.shape[0] // self.batch_size

    def __getitem__(self, index):
        """Returns one batch of data."""
        batch_indexes = self.indexes[index*self.batch_size:(index+1)*self.batch_size]
        batch_samples = [self.samples[i] for i in batch_indexes]
        X, y = self._make_batch(batch_samples)

        if self.model_type == 'dscnn' or self.model_type == 'cnn' or self.model_type == 'ds':
            X = X.reshape((X.shape[0], X.shape[1], X.shape[2], self.num_channels))
        if self.model_type == 'dnn':
            X = X.reshape((X.shape[0], X.shape[1], X.shape[2]))

        return X, y

    def _make_batch(self, samples_for_batch):
        """Constructs one batch of data."""
        # Initializing X and y
        X = np.empty((self.batch_size, *self.dim))
        y = np.empty((self.batch_size, len(TRAIN_TEST_LABELS)))

        # Constructing X and y
        for i, sample in enumerate(samples_for_batch):
            X[i] = np.load(sample)
            label = self.df.loc[sample, 'label']
            y[i] = self.lb.transform([label])

        return X, y

    def on_epoch_end(self):
        """Shuffles the sample order after each epoch"""
        self.indexes = np.arange(len(self.samples))
        if self.shuffle:
            np.random.shuffle(self.indexes)


if __name__ == '__main__':
    # Example code for testing
    df = get_dataframe(VER, "_features_clean")
    training_generator = AudioFeed(df[df['category'] == 'training'], model_type='cnn')
    testing_generator = AudioFeed(df[df['category'] == 'testing'], model_type='cnn')
    idx = 1
    testing_generator.__getitem__(idx)

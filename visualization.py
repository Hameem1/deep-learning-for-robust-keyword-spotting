"""Implements functions for creating data visualizations."""

import numpy as np
from scipy import signal
import plotly.offline as pyo
import plotly.graph_objects as go
from plotly.subplots import make_subplots
from config import DATA_FILES_DIR, DEFAULT_PLOTLY_COLORS


def plot_class_dist(df1=None, df2=None, filename="class_distribution.html", show=True):
    """
    Plots a bar chart for the class distribution of both ver 1 and ver 2 of the dataset.

    Parameters
    ----------
    df1 : DataFrame
        Version 1 of the google speech commands dataset.
    df2 : DataFrame
        Version 2 of the google speech commands dataset.
    filename : str
        The filename for the plot.
    show : bool
        Shows the plot if True, else the plot is generated in DATA_FILES_DIR.

    """

    assert (df1 is not None) or (df2 is not None), f"Please provide at least one DataFrame!"

    data = []
    if df1 is not None:
        # Getting the class distributions for the datasets
        class_dist_df1 = df1.groupby(['label'])['runtime'].sum()
        # Configuring the plot traces
        trace_df1 = go.Bar(name='ver 0.01', x=class_dist_df1.index, y=class_dist_df1.values)
        data.append(trace_df1)
    if df2 is not None:
        class_dist_df2 = df2.groupby(['label'])['runtime'].sum()
        trace_df2 = go.Bar(name='ver 0.02', x=class_dist_df2.index, y=class_dist_df2.values)
        data.append(trace_df2)

    # Configuring the plot layout
    layout = go.Layout(title={'text': 'Speech Commands Dataset - Word Distribution',
                              'y': 0.95,
                              'x': 0.5,
                              'xanchor': 'center',
                              'yanchor': 'top'},
                       xaxis_title="Word",
                       yaxis_title="Total Runtime (s)",
                       barmode='group',
                       font=dict(family='Open Sans', size=32, color='#171717'))

    # Combining the data and layout
    fig = go.Figure(data=data, layout=layout)
    # Plotting the graph
    pyo.plot(fig, filename=(DATA_FILES_DIR / filename).as_posix(), auto_open=show)


def plot_signal(audio_obj, plot_type, plot_title=None, show=True, return_plot=False):
    """
    Plots the given audio signal vs time (s), or it's FFT.

    Parameters
    ----------
    audio_obj : AudioFile
        The audio object.
    plot_type : str
        Type of plot ("signal", "fft").
    plot_title : str
        The title of the plot.
    show : bool
        Shows the plot if True, else the plot is only generated in DATA_FILES_DIR.
    return_plot : bool
        If True, the function returns the plotting parameters and doesn't generate the plot (used by other functions).

    """

    # Setting plotting parameters for the type of plot
    if plot_type == "signal":
        x = np.arange(len(audio_obj.signal)) / audio_obj.fs
        y = audio_obj.signal
        name = "raw signal"
        plot_title = plot_title if plot_title else f"Raw Signal : " + audio_obj.filepath.as_posix()
        x_axis_title = "Time (s)"
        y_axis_title = "Magnitude"
        filename = f"{audio_obj.filepath.stem}_signal.html"

    elif plot_type == "fft":
        x = audio_obj.fft[0]
        y = audio_obj.fft[1]
        name = "signal fft"
        plot_title = plot_title if plot_title else f"Signal FFT : " + audio_obj.filepath.as_posix()
        x_axis_title = "Frequency (Hz)"
        y_axis_title = "Magnitude"
        filename = f"{audio_obj.filepath.stem}_fft.html"
    else:
        raise ValueError(f"Invalid argument : '{plot_type}' - Allowed values are 'signal' & 'fft'")

    if return_plot:
        return x, y, plot_title

    data = [go.Scatter(x=x, y=y, mode='lines',
                       name=name, line=dict(color=np.random.choice(DEFAULT_PLOTLY_COLORS)))]
    layout = go.Layout(title={'text': plot_title,
                              'y': 0.95,
                              'x': 0.5,
                              'xanchor': 'center',
                              'yanchor': 'top'},
                       xaxis_title=x_axis_title,
                       yaxis_title=y_axis_title,
                       font=dict(family='Arial', size=20, color='#7f7f7f'),
                       plot_bgcolor="rgb(255, 255, 255)")

    # Combining the data and layout
    fig = go.Figure(data=data, layout=layout)
    # Plotting the graph
    pyo.plot(fig, filename=(DATA_FILES_DIR / 'temp' / filename).as_posix(), auto_open=show)


def plot_spectrogram(audio_obj, plot_type, plot_title=None, show=True, return_plot=False):
    """
    Plots a spectrogram for the audio signal, log-filter bank energies, or mel-frequency cepstral coefficients.

    Parameters
    ----------
    audio_obj : AudioFile
        The audio object.
    plot_type : str
        Type of plot ("signal", "log_f_bank", "mfcc").
    plot_title : str
        The title of the plot.
    show : bool
        Shows the plot if True, else the plot is only generated in DATA_FILES_DIR.
    return_plot : bool
        If True, the function returns the plotting parameters and doesn't generate the plot (used by other functions).

    """

    # Setting plotting parameters for the type of plot
    if plot_type == 'signal':
        freqs, bins, s_gram = signal.spectrogram(audio_obj.signal, audio_obj.fs, window='hann', nfft=512)
        x = bins
        y = freqs
        z = 10 * np.log10(s_gram)
        plot_title = f"Audio Spectrogram : " + (plot_title if plot_title else audio_obj.filepath.as_posix())
        y_axis_title = "Frequency (Hz)"
        filename_append = "signal_s_gram"

    elif plot_type == 'log_f_bank':
        x = np.linspace(0, audio_obj.runtime, num=audio_obj.log_f_bank.shape[1])
        y = np.arange(audio_obj.log_f_bank.shape[1])
        z = audio_obj.log_f_bank
        plot_title = f"Log-Filter Bank Spectrogram : " + (plot_title if plot_title else audio_obj.filepath.as_posix())
        y_axis_title = "Log-Filter Bank Coefficients (n)"
        filename_append = "signal_log_f_bank"

    elif plot_type == 'mfcc':
        x = np.linspace(0, audio_obj.runtime, num=audio_obj.mfcc.shape[1])
        y = np.arange(audio_obj.mfcc.shape[1])
        z = audio_obj.mfcc
        plot_title = f"MFCC Spectrogram : " + (plot_title if plot_title else audio_obj.filepath.as_posix())
        y_axis_title = "Mel-Frequency Cepstral Coefficients (n)"
        filename_append = "signal_mfcc"
    else:
        raise ValueError(f"Invalid argument : '{plot_type}' - Allowed values are 'signal', 'log_f_bank' & 'mfcc'")

    if return_plot:
        return x, y, z, plot_title

    data = [go.Heatmap(x=x,
                       y=y,
                       z=z,
                       colorscale='Hot',
                       )]

    layout = go.Layout(title={'text': plot_title,
                              'y': 0.95,
                              'x': 0.5,
                              'xanchor': 'center',
                              'yanchor': 'top'},
                       xaxis_title="Time (s)",
                       yaxis_title=y_axis_title,
                       font=dict(family='Arial', size=20, color='#7f7f7f'),
                       plot_bgcolor="rgb(255, 255, 255)")

    # Combining the data and layout
    fig = go.Figure(data=data, layout=layout)
    # Plotting the graph
    pyo.plot(fig, filename=(DATA_FILES_DIR / 'temp' / f'{audio_obj.filepath.stem}_{filename_append}.html').as_posix(),
             auto_open=show)


def get_subplots(audio_objs, cols=2):
    """
    Returns a Figure object for sub-plotting and a (row, col) assignment for each audio object within the sub-plot.

    Parameters
    ----------
    audio_objs : list of AudioFile
        A list of AudioFile objects which need to be plotted.
    cols : int
        The number of columns in the subplot.

    Returns
    -------
    fig : Plotly Figure object.
    rc : (row, col) assignment for each AudioFile object.

    """

    # Making sure that the input is a list of AudioFile objects
    assert isinstance(audio_objs, list), f"Input is not a list!"
    assert isinstance(audio_objs[0], AudioFile), f"Input is not a list of class objects!"
    assert len(audio_objs) <= 10, f"Max signals allowed is 10!"

    # Setting up an automatic grid of subplots for upto 10 audio instances
    rows = int(len(audio_objs) / cols)
    fig = make_subplots(rows=rows, cols=cols, subplot_titles=[ao.label for ao in audio_objs])
    r = (list(range(1, rows + 1)) * cols)
    r.sort()
    c = list(range(1, cols + 1)) * rows
    rc = [(r, c) for r, c in zip(r, c)]
    return fig, rc


def plot_multiple_signals(audio_objs, plot_type, title=None, filename=None, show=True):
    """
    Plots multiple audio signals vs time (s), or their FFTs as sub-plots.

    Parameters
    ----------
    audio_objs : list of AudioFile
        A list of AudioFile objects which need to be plotted.
    plot_type : str
        Type of plot ("signal", "fft").
    title : str
        The title of the plot.
    filename : str
        The filename for the plot.
    show : bool
        Shows the plot if True, else the plot is only generated in DATA_FILES_DIR.

    """

    # Getting the figure object and [row, col] assignment for each plot (within the subplot)
    fig, rc = get_subplots(audio_objs)
    # For each audio object, along with its grid position
    for audio_obj, (r, c) in zip(audio_objs, rc):
        x, y, plot_title = plot_signal(audio_obj, plot_type=plot_type, return_plot=True)
        # Adding a trace for the audio object
        fig.add_trace(go.Scatter(x=x, y=y, mode='lines',
                                 name=audio_obj.label,
                                 line=dict(color=np.random.choice(DEFAULT_PLOTLY_COLORS, replace=False))
                                 ), row=r, col=c)

    # Setting the plot title and filename
    title = title if title else plot_title.split(":")[0] + f"for {len(audio_objs)} words"
    filename = filename if filename else plot_title.split(":")[0].strip().replace(" ",
                                                                                  "_").lower() + f"_{len(audio_objs)}.html"
    # Setting up the layout for the graph
    fig.update_layout(title={'text': title,
                             'y': 0.95,
                             'x': 0.5,
                             'xanchor': 'center',
                             'yanchor': 'top'},
                      showlegend=False,
                      font=dict(family='Arial', size=17, color='#7f7f7f'),
                      plot_bgcolor="rgb(255, 255, 255)")
    # Plotting the graph
    pyo.plot(fig, filename=(DATA_FILES_DIR / filename).as_posix(), auto_open=show)


def plot_multiple_spectrograms(audio_objs, plot_type, title=None, filename=None, show=True):
    """
    Plots multiple spectrograms for audio signals, log filter bank energies, or mfccs, as sub-plots.

    Parameters
    ----------
    audio_objs : list of AudioFile
        A list of AudioFile objects which need to be plotted.
    plot_type : str
        Type of plot ("signal", "log_f_bank", "mfcc").
    title : str
        The title of the plot.
    filename : str
        The filename for the plot.
    show : bool
        Shows the plot if True, else the plot is only generated in DATA_FILES_DIR.

    """

    # Getting the figure object and [row, col] assignment for each plot (within the subplot)
    fig, rc = get_subplots(audio_objs)
    # For each audio object, along with its grid position
    for audio_obj, (r, c) in zip(audio_objs, rc):
        x, y, z, plot_title = plot_spectrogram(audio_obj, plot_type, return_plot=True)

        # Adding a trace for the audio object
        fig.add_trace(go.Heatmap(x=x,
                                 y=y,
                                 z=z,
                                 coloraxis="coloraxis",
                                 ), row=r, col=c)

    # Setting the plot title and filename
    title = title if title else plot_title.split(":")[0] + f"for {len(audio_objs)} words"
    filename = filename if filename else plot_title.split(":")[0].strip().replace(" ",
                                                                                  "_").lower() + f"_{len(audio_objs)}.html"

    # Setting up the layout for the graph
    fig.update_layout(title={'text': title,
                             'y': 0.95,
                             'x': 0.5,
                             'xanchor': 'center',
                             'yanchor': 'top'},
                      showlegend=False,
                      font=dict(family='Arial', size=20, color='#7f7f7f'),
                      plot_bgcolor="rgb(255, 255, 255)",
                      coloraxis={'colorscale': 'Hot'})

    # Plotting the graph
    pyo.plot(fig, filename=(DATA_FILES_DIR / filename).as_posix(), auto_open=show)


if __name__ == '__main__':

    from data_structure import AudioFile
    from pre_processing import get_dataframe

    df_1 = get_dataframe('v1')
    df_2 = get_dataframe('v2')

    # Class distribution of the original Datasets
    # -------------------------------------------------
    plot_class_dist(df1=df_1, df2=df_2, show=True, filename="class_distribution_temp.html")

    # Class distribution of the cleaned (processed) Dataset
    # -----------------------------------------------
    df = get_dataframe('v2', '_clean')
    plot_class_dist(df2=df, show=True, filename="class_distribution_temp.html")
    audio_classes = list(df['label'].unique())

    # Plotting the audio signals and ffts for n words
    # -----------------------------------------------
    # number of words to plot
    n_words = 10
    # list containing one sample for each of the n words
    n_samples = [df[df['label'] == word].sample(n=1, replace=False, random_state=100).index[0]
                 for word in audio_classes[:n_words]]
    # list containing corresponding audio objects for these n samples
    audio_objs = [AudioFile(sample, with_features=True) for sample in n_samples]
    # plotting the audio signals for n_words
    plot_multiple_signals(audio_objs, 'signal', show=True)
    # plotting the audio signal FFTs for n_words
    plot_multiple_signals(audio_objs, 'fft', show=True)
    # plotting audio signal spectrograms for n_words
    plot_multiple_spectrograms(audio_objs, 'signal', show=True)
    # plotting log filter bank spectrograms for n_words
    plot_multiple_spectrograms(audio_objs, 'log_f_bank', show=True)
    # plotting mfcc spectrograms for n_words
    plot_multiple_spectrograms(audio_objs, 'mfcc', show=True)

    # Getting an object for testing
    audio_obj = audio_objs[0]

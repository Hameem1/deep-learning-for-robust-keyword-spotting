"""Loads and tests the tf-lite models."""
import os
from config import TRAIN_TEST_LABELS, MODELS_DIR, NUM_AUDIO_CHANNELS, N_MFCC, N_FRAMES, DATA_FILES_DIR, FORCE_CPU
if FORCE_CPU:
    os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
import numpy as np
import pandas as pd
import tensorflow as tf
from pathlib import Path
from logger import logger
from time import time_ns, time
from models import display_device_info
from inference_ops import set_input, _make_batch, get_batch_accuracy
from data_ops import get_model_type, get_train_test_cases, get_label_binarizer, model_name_parser

# Test cases
testing_cases = [("clean",), ("noise",), ("heavy_noise",)]
# Source model directories
# tflite_model_dir = Path(MODELS_DIR / "tf-lite")
# quantized_model_dir = Path(MODELS_DIR / "quantized")
test_models_dir = MODELS_DIR / 'inference_test'
# Container for testing results
testing_results = {'model': [], 'model format': [], 'quantization': [], 'training_data': [],
                   'extra_training_config': [], 'epochs': [], 'testcase': [], 'accuracy': [],
                   'memory (MB)': [], 'inference time (sample)': [], 'inference time (batch)': []}

if __name__ == '__main__':
    lb = get_label_binarizer()
    # Batch size for testing/inference
    batch_size = 100

    # tflite_models = [tflite_model_dir / model for model in os.listdir(tflite_model_dir) if model.endswith(".tflite")]
    # quantized_models = [quantized_model_dir / model for model in os.listdir(quantized_model_dir) if
    #                     model.endswith(".tflite")]
    models_under_test = [test_models_dir / model for model in os.listdir(test_models_dir) if model.endswith(".tflite")]

    # Getting the testing DataFrames according to the testing cases
    testing_data = get_train_test_cases(testing_cases)

    # models to run inference on
    # models = list(tflite_models + quantized_models)
    # Displaying info for inference device
    display_device_info()

    for model in models_under_test:
        if model.as_posix().__contains__("quantized"):
            model_type, model_format, quant_type = get_model_type(model, quantized=True)
        else:
            model_type, model_format = get_model_type(model)
            quant_type = " "
        logger.info(f"type: {model_type}, format: {model_format}")

        interpreter = tf.lite.Interpreter(model_path=model.as_posix())
        # Resize the interpreter input to accept batch data
        input_details = interpreter.get_input_details()
        output_details = interpreter.get_output_details()

        # Reshaping according to model type
        if model_type == 'cnn' or model_type == 'dscnn' or model_type == 'ds-cnn':
            interpreter.resize_tensor_input(input_details[0]['index'],
                                            (batch_size, N_FRAMES, N_MFCC, NUM_AUDIO_CHANNELS))
        if model_type == 'dnn':
            interpreter.resize_tensor_input(input_details[0]['index'], (batch_size, N_FRAMES, N_MFCC))
        interpreter.resize_tensor_input(output_details[0]['index'], (batch_size, len(TRAIN_TEST_LABELS)))

        # Allocating tensors
        interpreter.allocate_tensors()

        for df_name, df in testing_data.items():
            logger.info("")
            logger.info(f'Evaluating model "{model.stem}" with "{df_name}" data.')
            # Getting only the 'testing' samples
            df = df[df['category'] == 'testing']
            # Getting the indexes for the DataFrame
            total_indexes = np.arange(df.shape[0])
            # Shuffling the indexes
            np.random.shuffle(total_indexes)
            # Making batches of batch_size
            batch_indexes = (total_indexes[index * batch_size:(index + 1) * batch_size] for index in
                             range(len(total_indexes) // batch_size))
            batch_accuracies = []
            batch_inference_times = []

            for batch in batch_indexes:
                batch_df = df.iloc[batch]
                X, y = _make_batch(batch_df, model_type)
                set_input(interpreter, X, batch=True)
                start = time_ns()
                interpreter.invoke()
                duration_ns = time_ns() - start
                batch_inference_times.append(duration_ns)
                op = interpreter.get_tensor(output_details[0]['index'])
                batch_accuracies.append(get_batch_accuracy(op, y))

            # Updating the test results
            name = model.stem.replace('tflite_', "")
            name = name.replace("quantized_", "")
            name = name.replace(f'_{quant_type}', "")
            _model_name, _training_data, _epochs, _training_config = model_name_parser(name)
            testing_results['model'].append(_model_name)
            testing_results['model format'].append(model_format)
            testing_results['quantization'].append(quant_type)
            testing_results['training_data'].append(_training_data)
            testing_results['extra_training_config'].append(_training_config)
            testing_results['memory (MB)'].append(round(os.path.getsize(model) / 1e6, 2))
            testing_results['epochs'].append(_epochs)
            testing_results['testcase'].append(df_name.replace("_", " "))
            testing_results['accuracy'].append(np.array(batch_accuracies).mean())
            testing_results['inference time (batch)'].append((np.array(batch_inference_times).mean()) / 1e6)
            testing_results['inference time (sample)'].append(
                ((np.array(batch_inference_times).mean()) / 1e6) / batch_size)

            logger.info(f"Model = '{model.stem}'")
            logger.info(f"Testcase = '{df_name}'")
            logger.info(f"Accuracy = {np.array(batch_accuracies).mean()}")
            logger.info(f"Avg inference time (per batch) = {(np.array(batch_inference_times).mean()) / 1e6} ms")
            logger.info(
                f"Avg inference time (per sample) = {((np.array(batch_inference_times).mean()) / 1e6) / batch_size} ms")

            batch_accuracies = []
            batch_inference_times = []

    logger.info(f"All models tested successfully.")
    results_df = pd.DataFrame(testing_results)
    results_df.to_csv(
        Path(DATA_FILES_DIR / "inference_results" / f"inference_results_complete_{int(time())}.csv"))
    logger.info(f"Results saved to .csv file.")

"""Trains and exports the NN models."""

import os
import re
import numpy as np
import pandas as pd
from time import time
from pathlib import Path
from logger import logger
from sklearn.utils import class_weight
from tensorflow.keras.callbacks import TensorBoard
from tensorflow.keras.callbacks import Callback as keras_Callback
from models import model_dnn, model_cnn, model_ds_cnn, _model_size
from data_generator import AudioFeed
from data_ops import get_train_test_cases, get_label_binarizer, model_name_parser
from config import BATCH_SIZE, MODELS_DIR, LOGS_DIR, N_FRAMES, N_MFCC, TRAIN_TEST_LABELS, MULTIPROCESSING, \
    DATA_FILES_DIR

# fraction of the dataset  (for development)
_divide_by = 1
batch_size = BATCH_SIZE
tensorboard = True
shuffle = True
weighted = False
# start with an underscore (e.g. "_shuffled") or leave empty string
custom_append = ""

# Getting the models to train on
models_to_train = [(model_ds_cnn, (N_FRAMES, N_MFCC, 1)),
                   (model_dnn, (N_FRAMES, N_MFCC)),
                   (model_cnn, (N_FRAMES, N_MFCC, 1))]
# The best performing data (augmented)
training_cases = [("clean", "noise", "heavy_noise", "time"), ("clean", "noise", "heavy_noise", "freq"),
                  ("clean", "noise", "heavy_noise"), ("clean", "heavy_noise"), ("clean",)]

training_results = {'model': [], 'training_data': [], 'extra_training_config': [], 'num_model_parameters': [],
                    'memory (MB)': [], 'iterations': [], 'epochs': [], 'training_accuracy': [], 'training_loss': [],
                    'validation_accuracy': [], 'validation_loss': []}

# Number of base iterations ~16K
_iteration_multiplier = 2
_comb_sizes = [len(tc) for tc in training_cases]
_lcm = np.lcm.reduce(_comb_sizes)
_epochs = np.array([_lcm // i for i in _comb_sizes]) * _iteration_multiplier
# Epochs list for ~33K iterations per model
training_epochs = _epochs.tolist()

assert len(training_cases) == len(training_epochs), \
    f"Size of 'training_cases' ({len(training_cases)}) != 'training_epochs' ({len(training_epochs)})"


class LossHistory(keras_Callback):
    def on_train_begin(self, logs={}):
        self.losses = []
        self.accuracies = []

    def on_batch_end(self, batch, logs={}):
        self.losses.append(logs.get('loss'))
        self.accuracies.append(logs.get('accuracy'))


history = LossHistory()

if __name__ == '__main__':

    lb = get_label_binarizer()
    # Getting the training data according to the training cases
    training_data = get_train_test_cases(training_cases)
    # for every model
    for model_tuple in models_to_train:
        # for every training case (zipped with epochs list)
        for data, n_epochs in zip(training_data.items(), training_epochs):
            # Getting a model from the model pool
            func, params = model_tuple
            model = func(sample_shape=params)
            # Getting the model type to have the input data shaped accordingly
            model_type = model.name.split("-")[0].lower()
            df_name, df = data
            filename = f"{model.name}_{df_name.replace('_', '-')}_{n_epochs}{custom_append}_{int(time())}"
            # Checking if the trained model already exists
            _files = [f for f in os.listdir(MODELS_DIR / 'regular') if f.endswith(".h5")]
            _model_type, _train_data, _train_epochs, *_ = filename.split("_")
            _partial_name = "_".join([_model_type, _train_data, _train_epochs])
            if any([re.search(rf"{_partial_name}_[0-9a-zA-Z_]*{custom_append}.h5", f, re.IGNORECASE) for f in _files]):
                logger.warning(f"The trained model '{filename}' already exists!")
                continue

            # Logging data for each run
            logger.info(f"Model : {model.name}")
            logger.info(f"Data : {df_name}")
            logger.info(f"Epochs : {n_epochs}")
            logger.info(f"Batch Size : {batch_size}")
            logger.info(f"No. of Training Samples : {df[df['category'] == 'training'].shape[0]}")
            if custom_append:
                logger.info(f"Extra info : '{custom_append}'")

            # Setting up the training and evaluation data generators
            training_generator = AudioFeed(df[df['category'] == 'training'].sample(frac=1 / int(_divide_by)),
                                           model_type=model_type,
                                           batch_size=batch_size, shuffle=shuffle)
            validation_generator = AudioFeed(df[df['category'] == 'validation'].sample(frac=1 / int(_divide_by)),
                                             model_type=model_type,
                                             batch_size=batch_size, shuffle=shuffle)

            # Setting up TensorBoard to log model training/testing
            if tensorboard:
                tensorboard = [TensorBoard(log_dir=Path(LOGS_DIR / f"tensorboard_logs/{filename}"))]
            else:
                tensorboard = None

            # Computing class weights
            if weighted:
                y = df[df['category'] == 'training']['label']
                y_ints = [i.argmax() for i in lb.transform(y)]
                class_weights = class_weight.compute_class_weight('balanced',
                                                                  np.array(range(len(TRAIN_TEST_LABELS))),
                                                                  y_ints)
            else:
                class_weights = None

            # Compiling the model
            model.compile(optimizer='adam',
                          loss='categorical_crossentropy',
                          metrics=['accuracy'])

            # Printing out the model summary
            model.summary(print_fn=logger.info)

            # Training/Testing the model
            val_history = model.fit(x=training_generator,
                                    validation_data=validation_generator,
                                    epochs=n_epochs,
                                    verbose=1,
                                    use_multiprocessing=MULTIPROCESSING,
                                    workers=8,
                                    callbacks=[tensorboard[0], history],
                                    class_weight=class_weights)

            # Saving the Model
            model_path = Path(MODELS_DIR / "regular" / f"{filename}.h5")
            model.save(model_path)
            # Updating the training results
            _model_name, _training_data, _epochs, _training_config = model_name_parser(model_path.stem)
            training_results['model'].append(_model_name)
            training_results['training_data'].append(_training_data)
            training_results['extra_training_config'].append(_training_config)
            training_results['num_model_parameters'].append(_model_size(model))
            training_results['memory (MB)'].append(round(os.path.getsize(model_path) / 1e6, 2))
            training_results['iterations'].append(len(training_generator)*n_epochs)
            training_results['epochs'].append(_epochs)
            training_results['training_accuracy'].append((np.array(history.accuracies) * 100).round(3).tolist())
            training_results['training_loss'].append(history.losses)
            training_results['validation_loss'].append(val_history.history.get('loss'))
            training_results['validation_accuracy'].append((np.array(val_history.history.get('accuracy'))*100).round(3).tolist())

    logger.info(f"All models trained successfully.")
    results_df = pd.DataFrame(training_results)
    if not results_df.empty:
        results_df.to_csv(Path(DATA_FILES_DIR / "training_results" / f'training_results_{int(time())}.csv'))
        logger.info(f"Results saved to .csv file.")

# Imports
from pathlib import Path
from logging import INFO, DEBUG

# Feature switches (to turn on/off different features)
VER = "v2"
DATA_AUGMENTATION = True
GENERATE_FEATURES = True
MULTIPROCESSING = False
AUGMENTS = [
    dict(transform="time", time_stretch=(0.85, 1.15)),
    dict(transform="freq", freq_shift=(-2, 2)),
    dict(transform="noise", noise_ratio=(0.3, 0.4)),
    dict(transform="heavy_noise", noise_ratio=(0.4, 0.5), noise_path=Path("./dataset_raw/heavy_noise"))
]
FORCE_CPU = False

# Feature Extraction Parameters #
NUM_AUDIO_CHANNELS = 1
FRAME_LEN = 0.04        # in ms
FRAME_STRIDE = 0.02     # in ms
SAMPLE_LEN = 1.0        # in sec
_frame_overlap = FRAME_LEN - FRAME_STRIDE
N_FRAMES = int((SAMPLE_LEN - _frame_overlap) / (FRAME_LEN - _frame_overlap))
N_MFCC = 20
N_FFT = 640
N_FILT = 26

# Training Parameters #
EPOCHS = 20
BATCH_SIZE = 64
TEST_PERCENT = 20
VALIDATE_PERCENT = 10
TRAIN_TEST_LABELS = sorted(
    ['yes', 'no', 'on', 'off', 'right', 'left', 'up', 'down', 'stop', 'go', 'forward', 'backward',
     'follow', 'unknown_word', 'silence'])
UNKNOWN_WORDS = sorted(['bed', 'bird', 'cat', 'dog', 'eight', 'five', 'four', 'happy', 'house', 'learn', 'marvin',
                        'nine', 'one', 'seven', 'sheila', 'six', 'three', 'tree', 'two', 'visual', 'wow', 'zero'])

# Path Variables #
# Dataset path
ROOT_DIR = Path(".")
LOGS_DIR = Path("./logs")
MODELS_DIR = Path("./models")
BENCHMARK_DIR = Path("./benchmark")
DATA_FILES_DIR = Path("./data_files")
DATAFRAMES_DIR = Path("./dataframes")
DATASET_DIR = Path("./dataset_raw")
DATASET_PROCESSED_DIR = Path("./dataset_processed")
AUGMENTED_DIR = "augmented_data"
DATA_PATH_v1 = DATASET_DIR / "speech_commands_v0.01"
DATA_PATH_v2 = DATASET_DIR / "speech_commands_v0.02"
DATA_PATH_PROCESSED_v1 = DATASET_PROCESSED_DIR / "speech_commands_v0.01"
DATA_PATH_PROCESSED_v2 = DATASET_PROCESSED_DIR / "speech_commands_v0.02"
URL_DATASET_V1 = "http://download.tensorflow.org/data/speech_commands_v0.01.tar.gz"
URL_DATASET_V2 = "http://download.tensorflow.org/data/speech_commands_v0.02.tar.gz"

# Logging Parameters #
# the ADMIN log file only gets one specific log level
HISTORY_LOG_FILTER = INFO
CURRENT_LOG_LEVEL = DEBUG
CONSOLE_LOG_LEVEL = DEBUG
HISTORY_LOG_FILE = 'history.log'
CURRENT_LOG_FILE = 'current.log'

# Temporary Configs #
REGENERATE = False
LABELS = ['one', 'two', 'three']
EXCLUDED_WORDS = []

# Plotly Colors
DEFAULT_PLOTLY_COLORS = ['rgb(31, 119, 180)', 'rgb(255, 127, 14)',
                         'rgb(44, 160, 44)', 'rgb(214, 39, 40)',
                         'rgb(148, 103, 189)', 'rgb(140, 86, 75)',
                         'rgb(227, 119, 194)', 'rgb(127, 127, 127)',
                         'rgb(188, 189, 34)', 'rgb(23, 190, 207)',
                         'forestgreen', 'indianred', 'indigo', 'firebrick', 'palegoldenrod']

ALL_PLOTLY_COLORS = "aliceblue,antiquewhite,aqua,aquamarine,azure,beige,bisque,black,blanchedalmond,blue,blueviolet," \
                    "brown,burlywood,cadetblue,chartreuse,chocolate,coral,cornflowerblue,cornsilk,crimson,cyan," \
                    "arkblue,darkcyan,darkgoldenrod,darkgray,darkgrey,darkgreen,darkkhaki,darkmagenta,darkolivegreen," \
                    "darkorange,darkorchid,darkred,darksalmon,darkseagreen,darkslateblue,darkslategray,darkslategrey," \
                    "darkturquoise,darkviolet,deeppink,deepskyblue,dimgray,dimgrey,dodgerblue,firebrick,floralwhite," \
                    "forestgreen,fuchsia,gainsboro,ghostwhite,gold,goldenrod,gray,grey,green,greenyellow,honeydew," \
                    "hotpink,indianred,indigo,ivory,khaki,lavender,lavenderblush,lawngreen,lemonchiffon,lightblue," \
                    "lightcoral,lightcyan,lightgoldenrodyellow,lightgray,lightgrey,lightgreen,lightpink,lightsalmon," \
                    "lightseagreen,lightskyblue,lightslategray,lightslategrey,lightsteelblue,lightyellow,lime,limegreen," \
                    " linen,magenta,maroon,mediumaquamarine,mediumblue,mediumorchid,mediumpurple,mediumseagreen," \
                    "mediumslateblue,mediumspringgreen,mediumturquoise,mediumvioletred,midnightblue,mintcream,mistyrose," \
                    "moccasin,navajowhite,navy,oldlace,olive,olivedrab,orange,orangered,orchid,palegoldenrod," \
                    "palegreen,paleturquoise,palevioletred,papayawhip,peachpuff,peru,pink,plum,powderblue,purple,red," \
                    "rosybrown,royalblue,saddlebrown,salmon,sandybrown,seagreen,seashell,sienna,silver,skyblue," \
                    "slateblue,slategray,slategrey,snow,springgreen,steelblue,tan,teal,thistle,tomato,turquoise," \
                    "violet,wheat,white,whitesmoke,yellow,yellowgreen".split(',')

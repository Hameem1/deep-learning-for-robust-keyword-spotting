# Deep Learning for Robust Keyword Spotting in the Edge

Keyword spotting (KWS) is an integral part of speech recognition systems designed for user interaction. KWS is mostly used to detect an initial “wake word” which acts as a trigger to receive further user input in the form of speech. This keyword detection needs to happen on-device and needs to be robust in order to offer a smooth user experience. In the real world, however, environmental noise can prove to hinder the performance of KWS models in a significant manner. In addition, microcontrollers which are used for model inference due to their attractive size, cost and power requirements, suffer from relatively slow inference times.  This is due to the real-time requirement and always-on nature of KWS systems which requires continuous inferencing, therefore limiting the number of operations per inference. In this study, a limited keyword vocabulary is used to design a voice-based interface for machine control. Different neural network architectures from previous works are trained using the open source google speech commands dataset, and the effects of data augmentation in building noise robust models is analyzed for different augments of the original data. The models are then evaluated on test data which ranges from clean to heavily noisy. The effects of model compression – using different quantization schemes – on accuracy, inference speed and model size are also explored. Furthermore, inference times are benchmarked on different hardware platforms to showcase model inference speed for embedded hardware. By using an optimized combination of clean, noisy and heavily noisy data augments, an accuracy of 95.9% is achieved with the DS-CNN model architecture, post full-integer (INT8) quantization. It is found that the inference speed for an implementation of this model architecture (post-quantization) on the edge TPU is more than 11 times faster than on the Raspberry Pi 3, and very much on par with desktop grade performance. The size of this model is around 600KB, which is more than 9 times smaller than the original model.


## Overview

* This project is developed using Python 3.7.
* Both module and project level configuration variables are provided for modifying code behavior.
* The project is compatible with both Windows and Linux systems.
* The function docstrings follow the "numpydoc" format (https://numpydoc.readthedocs.io/en/latest/format.html).
* The dataset used in this implementation comes from : 
    "http://download.tensorflow.org/data/speech_commands_v0.02.tar.gz"

## Installation:

**1)** Clone this repository to your local machine.

**2)** Create a Python virtual environment in the project root and install the packages in "requirements.txt".
```
$ python3 -m venv venv

# for windows
$ source venv/Scripts/activate
# for linux
$ source venv/bin/activate

$ pip install -r requirements.txt
```

**3)** Run `python pre_processing.py`. This is required on first run, as it automatically downloads the required dataset and performs all the pre processing steps.

**4)** Before running any other module, make sure it follows the sequence of the processing pipeline. More information is available in the project report and within the code documentation.

---

### Note:

- In order to run the notebooks, copy the relevant notebook from the "notebooks" directory to the project's root directory.
- Read the in-code documentation to follow along with what is being done.

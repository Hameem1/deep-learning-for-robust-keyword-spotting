"""Performs an inference time benchmark for the given models."""

import os
from config import DATA_FILES_DIR, BENCHMARK_DIR, FORCE_CPU
if FORCE_CPU:
    os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
import numpy as np
import pandas as pd
import tensorflow as tf
from pathlib import Path
from logger import logger
from time import time_ns, time
from models import display_device_info
from inference_ops import set_input, _make_batch, get_edge_tpu_lib
from data_ops import get_model_type, model_name_parser

EDGE_TPU = False
# number of samples to the get mean inference time from
n_avg = 1000
# Optional append (e.g., device name)
filename_append = ""
# Place the models to benchmark in the 'models' directory inside BENCHMARK_DIR
benchmark_models = Path(BENCHMARK_DIR / 'models')
# Getting the benchmarking data
benchmark_data = Path(BENCHMARK_DIR / 'data')
# Container for testing results
testing_results = {'model': [], 'model format': [], 'quantization': [], 'memory (MB)': [],
                   'average inference time (ms)': []}
# Getting the edge-tpu library for using the TPU to run the inference
edge_tpu_lib = get_edge_tpu_lib()

if __name__ == '__main__':

    model_paths = sorted(
        benchmark_models / model for model in os.listdir(benchmark_models) if model.endswith(".tflite"))
    files = [(benchmark_data / filename).as_posix() for filename in os.listdir(benchmark_data)]
    df = pd.DataFrame(dict(file_paths=files)).set_index('file_paths')
    samples = df.sample(n_avg)

    display_device_info()
    logger.info(f"Running Inference Time Benchmark...")
    logger.info(f"Calculating the average inference time over {len(samples)} samples.\n\n")

    for model_path in model_paths:
        # Getting model info
        if model_path.as_posix().__contains__("quantized"):
            model_type, model_format, quant_type = get_model_type(model_path, quantized=True)
        else:
            model_type, model_format = get_model_type(model_path)
            quant_type = " "
        logger.info(f"model: {model_type}, format: {model_format}"
                    f"{(', quantization: ' + quant_type) if len(quant_type) > 1 else ''}")
        # Creating an interpreter for inference
        if not EDGE_TPU:
            interpreter = tf.lite.Interpreter(model_path=model_path.as_posix())
        else:
            interpreter = tf.lite.Interpreter(model_path=model_path.as_posix(),
                                              experimental_delegates=[tf.lite.load_delegate(edge_tpu_lib)])
        # Getting i/p and o/p details for the interpreter
        input_details = interpreter.get_input_details()
        output_details = interpreter.get_output_details()
        # Allocating tensors
        interpreter.allocate_tensors()
        # The data for benchmarking
        X = _make_batch(samples, model_type, ignore_labels=True)
        # Conversion factor (ns to ms)
        _divide_by = float(1e6)
        # Contains the inference times per sample
        inference_times = []
        for i in range(samples.shape[0]):
            ip = set_input(interpreter, X[i])
            start = time_ns()
            interpreter.invoke()
            duration_ns = time_ns() - start
            inference_times.append(duration_ns)
            op = interpreter.get_tensor(output_details[0]['index'])

        # Updating the test results
        name = model_path.stem.replace('tflite_', "")
        name = name.replace("quantized_", "")
        name = name.replace(f'_{quant_type}', "")
        _model_name, _training_data, _epochs, _training_config = model_name_parser(name)
        avg_inference_time = round((np.array(inference_times).mean()) / _divide_by, 5)
        testing_results['model'].append(_model_name)
        testing_results['model format'].append(model_format)
        testing_results['quantization'].append(quant_type)
        testing_results['memory (MB)'].append(round(os.path.getsize(model_path) / 1e6, 2))
        testing_results['average inference time (ms)'].append(avg_inference_time)
        logger.info(f"Avg inference time = {avg_inference_time} ms\n")

    logger.info("")
    logger.info(f"All models benchmarked successfully.")
    results_df = pd.DataFrame(testing_results)
    results_df.to_csv(Path(
        DATA_FILES_DIR / "inference_results" / f'inference_benchmark_{"_" + filename_append if filename_append else ""}'
                                               f'{int(time())}.csv'))
    logger.info(f"Results saved to .csv file.")

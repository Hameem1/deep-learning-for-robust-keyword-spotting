"""Loads and tests the NN models (Keras)."""

import os
from config import MODELS_DIR, BATCH_SIZE, DATA_FILES_DIR, MULTIPROCESSING, FORCE_CPU
if FORCE_CPU:
    os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
import numpy as np
import pandas as pd
from time import time_ns, time
from pathlib import Path
from logger import logger
from models import _model_size
from data_generator import AudioFeed
from tensorflow.keras.models import load_model
from data_ops import get_train_test_cases, model_name_parser
from inference_ops import _make_batch


# For optional logging info
custom_append = ""
# Test cases
testing_cases = [("clean",), ("noise",), ("heavy_noise",)]
# Source directory for the Keras models
keras_models_dir = Path(MODELS_DIR / "regular/run-8")
# Container for testing results
testing_results = {'model': [], 'training_data': [], 'extra_training_config': [], 'num_model_parameters': [],
                   'epochs': [], 'testcase': [], 'accuracy': [], 'loss': [], 'memory (MB)': [],
                   'inference time (sample)': []}


if __name__ == '__main__':

    # Getting the testing DataFrames according to the testing cases
    testing_data = get_train_test_cases(testing_cases)
    # Getting the models under test
    keras_models = [keras_models_dir / model for model in os.listdir(keras_models_dir) if model.endswith(".h5")]

    for model_path in keras_models:
        model = load_model(model_path.as_posix())
        model_type = model.name.split("-")[0].lower()
        for df_name, df in testing_data.items():
            # Logging data for each run
            logger.info("")
            logger.info(f"Model : {model.name}")
            logger.info(f"Data : {df_name}")
            logger.info(f"No. of testing Samples : {df[df['category'] == 'testing'].shape[0]}")
            if custom_append:
                logger.info(f"Extra info : '{custom_append}'")

            # Getting the testing generator
            testing_generator = AudioFeed(df[df['category'] == 'testing'], model_type=model_type, batch_size=BATCH_SIZE)

            # Model Evaluation
            logger.info(f'Evaluating model "{model_path.stem}" with "{df_name}"')
            loss, acc = model.evaluate(x=testing_generator,
                                       verbose=1,
                                       use_multiprocessing=MULTIPROCESSING,
                                       workers=8)
            logger.info(f"Model Loss = {loss}, Model Accuracy = {acc * 100}%")

            logger.info("Testing model latency...")
            batch_size = 1
            df_test = df[df['category'] == 'testing'].sample(1000)
            # Getting the indexes for the DataFrame
            total_indexes = np.arange(df_test.shape[0])
            # Shuffling the indexes
            np.random.shuffle(total_indexes)
            # Making batches of batch_size
            batch_indexes = (total_indexes[index * batch_size:(index + 1) * batch_size] for index in
                             range(len(total_indexes) // batch_size))
            batch_inference_times = []

            for batch in batch_indexes:
                batch_df = df_test.iloc[batch]
                X, _ = _make_batch(batch_df, model_type)
                start = time_ns()
                model.predict(X, batch_size=batch_size)
                duration_ns = time_ns() - start
                batch_inference_times.append(duration_ns)

            inf_time_batch = (np.array(batch_inference_times).mean()) / 1e6
            inf_time_sample = round(inf_time_batch / batch_size, 2)
            logger.info(f"Inference Time per Sample = {inf_time_sample} ms\n")

            # Updating the test results
            _model_name, _training_data, _epochs, _training_config = model_name_parser(model_path.stem)
            testing_results['model'].append(_model_name)
            testing_results['training_data'].append(_training_data)
            testing_results['extra_training_config'].append(_training_config)
            testing_results['num_model_parameters'].append(_model_size(model))
            testing_results['memory (MB)'].append(round(os.path.getsize(model_path) / 1e6, 2))
            testing_results['epochs'].append(_epochs)
            testing_results['testcase'].append(df_name.replace("_", " "))
            testing_results['accuracy'].append(acc * 100)
            testing_results['loss'].append(loss)
            testing_results['inference time (sample)'].append(inf_time_sample)

    logger.info(f"All models tested successfully.")
    results_df = pd.DataFrame(testing_results)
    logger.info(f"Results saved to .csv file.")
    results_df.to_csv(Path(DATA_FILES_DIR / "testing_results" / f'testing_results_complete_{int(time())}.csv'))

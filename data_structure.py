"""Contains a data structure (class) for data/functionality relevant to an Audio file."""

# Imports
import librosa
import numpy as np
from pathlib import Path
import simpleaudio as sa
from numpy.fft import rfft, rfftfreq
from python_speech_features import mfcc, logfbank
from visualization import plot_signal, plot_spectrogram
from config import FRAME_LEN, FRAME_STRIDE, N_FRAMES, N_MFCC, N_FFT, N_FILT


class AudioFile:
    """
    This class contains the signal data and associated methods for a single audio file.

    Parameters
    ----------
    file_path : str or Path
        The path to the audio file.
    with_features: bool
        if True, calculates the fft, log_f_bank and mffc features on object instantiation.

    Attributes
    ----------
    signal : np.array
        The floating point audio data.
    fs : int
        The sampling frequency of the audio file.
    runtime : float
        The length of the audio file in seconds.
    filepath : Path
        The path to the audio file.
    filename : str
        The name of the audio file.
    label : str
        The label of the audio (word or sound).
    fft : np.array
        The fourier transform of the signal.
    log_f_bank : np.array
        The log filter bank energies for the signal.
    mfcc : np.array
        The Mel Frequency Cepstral Coefficients.

    Methods
    -------
    calc_fft()
        Calculates the fourier transform of the audio signal.
    calc_log_f_bank()
        Calculates the log filter-bank frequencies for the audio signal.
    calc_mfcc()
        Calculates the mel-frequency cepstral coefficients  for the audio signal.
    plot_signal()
        Plots the raw audio signal vs time (s).
    plot_fft()
        Plots the signal FFT.
    plot_spectrum()
        Plots the audio signal spectrogram.
    plot_log_f_bank()
        Plots a spectrogram for the Log-filter Bank energies.
    plot_mfcc()
        Plots a spectrogram for the Mel-Frequency Ceptral Coefficients.

    """

    def __init__(self, file_path, with_features=False):
        self.signal, self.fs = librosa.load(file_path, sr=None, mono=True)
        self.runtime = self.signal.shape[0] / self.fs
        self.filepath = Path(file_path)
        self.filename = Path(file_path).name
        self.label = Path(file_path).parts[-2]
        self.fft = None
        self.log_f_bank = None
        self.mfcc = None
        if with_features:
            self.calc_fft()
            self.calc_log_f_bank()
            self.calc_mfcc()

    def calc_fft(self):
        n = len(self.signal)
        # The frequency bin centers in cycles per unit of the sample spacing (time), since sample spacing is 1/fs = T.
        # The rfft method provides an almost x2 speed boost over fft for real valued signals.
        freq = rfftfreq(n, d=1 / self.fs)
        # Dividing by n to normalize for the length of the signal.
        mag = abs(rfft(self.signal) / n)
        self.fft = freq, mag

    def calc_log_f_bank(self):
        self.log_f_bank = logfbank(signal=self.signal,
                                   samplerate=self.fs,
                                   winlen=FRAME_LEN,
                                   winstep=FRAME_STRIDE,
                                   nfilt=N_FILT,
                                   nfft=N_FFT).T

    def calc_mfcc(self):
        temp_mfcc = mfcc(signal=self.signal,
                         samplerate=self.fs,
                         winlen=FRAME_LEN,
                         winstep=FRAME_STRIDE,
                         numcep=N_MFCC,
                         nfilt=N_FILT,
                         nfft=N_FFT)
        # reshaping to always have (N_FRAMES x N_MFFC)
        if temp_mfcc.shape[0] < N_FRAMES:
            pad_width = N_FRAMES - temp_mfcc.shape[0]
            self.mfcc = np.pad(temp_mfcc, pad_width=((0, pad_width), (0, 0)), mode='constant')
        else:
            self.mfcc = temp_mfcc[:N_FRAMES, :]

    def plot_signal(self):
        plot_signal(self, "signal")

    def plot_fft(self):
        plot_signal(self, "fft")

    def plot_spectrogram(self):
        plot_spectrogram(self, plot_type='signal')

    def plot_log_f_bank(self):
        plot_spectrogram(self, plot_type='log_f_bank')

    def plot_mfcc(self):
        plot_spectrogram(self, plot_type='mfcc')

    def play_audio(self):
        wav_obj = sa.WaveObject.from_wave_file(self.filepath.as_posix())
        play_obj = wav_obj.play()
        play_obj.wait_done()


if __name__ == '__main__':
    # Code for testing
    from pre_processing import get_dataframe
    from config import VER

    df = get_dataframe(VER, df_type="_clean")

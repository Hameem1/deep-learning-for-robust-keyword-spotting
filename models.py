"""Contains the Neural Network models, and performs model format conversion and quantization."""

import numpy as np
import tensorflow as tf
from pathlib import Path
from logger import logger
from pre_processing import get_dataframe
from data_ops import get_model_type
from config import TRAIN_TEST_LABELS, NUM_AUDIO_CHANNELS, VER, MODELS_DIR

config = tf.compat.v1.ConfigProto()
config.gpu_options.allow_growth = True  # dynamically grow the memory used on the GPU
config.log_device_placement = True  # to log device placement (on which device the operation ran)
sess = tf.compat.v1.Session(config=config)
tf.compat.v1.keras.backend.set_session(sess)  # set this TensorFlow session as the default session for Keras

# Number of output neurons
_n_outputs = len(TRAIN_TEST_LABELS)
# Private variables
_data, _model_type = None, None


def model_dnn(sample_shape):
    """Returns a fully connected Deep Neural network (DNN)."""

    inputs = tf.keras.Input(shape=sample_shape)
    x = tf.keras.layers.Flatten()(inputs)
    x = tf.keras.layers.Dense(128, activation='relu')(x)
    x = tf.keras.layers.Dropout(0.5)(x)
    x = tf.keras.layers.Dense(128, activation='relu')(x)
    x = tf.keras.layers.Dense(128, activation='relu')(x)
    outputs = tf.keras.layers.Dense(_n_outputs, activation='softmax')(x)
    model = tf.keras.Model(inputs=inputs, outputs=outputs, name="DNN-3-layer")
    return model


def model_cnn(sample_shape):
    """Returns a Convolutional Neural network (CNN)."""

    inputs = tf.keras.layers.Input(shape=sample_shape)
    x = tf.keras.layers.Conv2D(16, (3, 3), activation='relu', strides=(1, 1), padding='same')(inputs)
    x = tf.keras.layers.Conv2D(32, (3, 3), activation='relu', strides=(1, 1), padding='same')(x)
    x = tf.keras.layers.Conv2D(64, (3, 3), activation='relu', strides=(1, 1), padding='same')(x)
    # x = tf.keras.layers.Conv2D(128, (3, 3), activation='relu', strides=(1, 1), padding='same')(x)
    x = tf.keras.layers.MaxPool2D((2, 2))(x)
    x = tf.keras.layers.Dropout(0.5)(x)
    x = tf.keras.layers.Flatten()(x)
    x = tf.keras.layers.Dense(64, activation='relu')(x)
    x = tf.keras.layers.Dense(32, activation='relu')(x)
    outputs = tf.keras.layers.Dense(_n_outputs, activation='softmax')(x)
    model = tf.keras.Model(inputs=inputs, outputs=outputs, name="CNN")
    return model


def model_ds_cnn(sample_shape):
    """Returns a Depthwise Separable Convolutional Neural network (CNN)."""

    inputs = tf.keras.layers.Input(shape=sample_shape)
    x = tf.keras.layers.Conv2D(32, (3, 3), strides=(2, 2), padding='same', name='3x3-Conv')(inputs)
    x = tf.keras.layers.BatchNormalization()(x)
    x = tf.keras.activations.relu(x)

    x = tf.keras.layers.DepthwiseConv2D((3, 3), strides=(1, 1), padding='same', depth_multiplier=1,
                                        name='Depthwise-Conv-1')(x)
    x = tf.keras.layers.BatchNormalization()(x)
    x = tf.keras.activations.relu(x)
    x = tf.keras.layers.Conv2D(64, (1, 1), strides=(1, 1), padding='same', name='Pointwise-Conv-1')(x)
    x = tf.keras.layers.BatchNormalization()(x)
    x = tf.keras.activations.relu(x)

    x = tf.keras.layers.DepthwiseConv2D((3, 3), strides=(2, 2), padding='same', depth_multiplier=1,
                                        name='Depthwise-Conv-2')(x)
    x = tf.keras.layers.BatchNormalization()(x)
    x = tf.keras.activations.relu(x)
    x = tf.keras.layers.Conv2D(128, (1, 1), strides=(1, 1), padding='same', name='Pointwise-Conv-2')(x)
    x = tf.keras.layers.BatchNormalization()(x)
    x = tf.keras.activations.relu(x)

    x = tf.keras.layers.DepthwiseConv2D((3, 3), strides=(1, 1), padding='same', depth_multiplier=1,
                                        name='Depthwise-Conv-3')(x)
    x = tf.keras.layers.BatchNormalization()(x)
    x = tf.keras.activations.relu(x)
    x = tf.keras.layers.Conv2D(128, (1, 1), strides=(1, 1), padding='same', name='Pointwise-Conv-3')(x)
    x = tf.keras.layers.BatchNormalization()(x)
    x = tf.keras.activations.relu(x)

    x = tf.keras.layers.DepthwiseConv2D((3, 3), strides=(2, 2), padding='same', depth_multiplier=1,
                                        name='Depthwise-Conv-4')(x)
    x = tf.keras.layers.BatchNormalization()(x)
    x = tf.keras.activations.relu(x)
    x = tf.keras.layers.Conv2D(256, (1, 1), strides=(1, 1), padding='same', name='Pointwise-Conv-4')(x)
    x = tf.keras.layers.BatchNormalization()(x)
    x = tf.keras.activations.relu(x)

    for i in range(5):
        x = tf.keras.layers.DepthwiseConv2D((3, 3), strides=(1, 1), padding='same', depth_multiplier=1,
                                            name=f'Depthwise-Conv-{5 + i}')(x)
        x = tf.keras.layers.BatchNormalization()(x)
        x = tf.keras.activations.relu(x)
        x = tf.keras.layers.Conv2D(256, (1, 1), strides=(1, 1), padding='same', name=f'Pointwise-Conv-{5 + i}')(x)
        x = tf.keras.layers.BatchNormalization()(x)
        x = tf.keras.activations.relu(x)

    x = tf.keras.layers.AvgPool2D((2, 2))(x)
    x = tf.keras.layers.Flatten()(x)
    x = tf.keras.layers.Dense(32, activation='relu')(x)
    outputs = tf.keras.layers.Dense(_n_outputs, activation='softmax')(x)
    model = tf.keras.Model(inputs=inputs, outputs=outputs, name="DSCNN")
    return model


def model_to_tflite(model_path, target_path, quantize=False, quant_type=None, repr_data_gen=None,
                    filename_append=""):
    """Converts a Keras model to a TensorFlow Lite model."""

    new_model_name = f"tflite_{model_path.stem}{'_quantized' if quantize else ''}{filename_append}.tflite"
    new_model_path = Path(target_path / new_model_name)
    if not new_model_path.exists():
        model = tf.keras.models.load_model(filepath=model_path)
        # Converting the model to a tflite model (quantized)
        converter = tf.lite.TFLiteConverter.from_keras_model(model)
        if quantize:
            if quant_type == '8-bit':
                converter.optimizations = [tf.lite.Optimize.OPTIMIZE_FOR_SIZE]
            if quant_type == 'float-16':
                converter.optimizations = [tf.lite.Optimize.DEFAULT]
                converter.target_spec.supported_types = [tf.float16]
            if quant_type == 'full-int':
                converter.optimizations = [tf.lite.Optimize.OPTIMIZE_FOR_SIZE]
                assert (repr_data_gen is not None), \
                    f"Representative data generator needed for '{quant_type}' quantization!"
                converter.representative_dataset = repr_data_gen
        tflite_model = converter.convert()
        logger.info(f"Model {'quantized and ' if quantize else ''}successfully converted to TensorFlow lite format.")
        with open(new_model_path.as_posix(), "wb") as new_model:
            new_model.write(tflite_model)
        logger.info(f"Model saved :  {new_model_name}")
        del model, converter, tflite_model
    else:
        logger.info(f"The converted model already exists for {model_path.stem}")
    return new_model_path


def representative_data():
    global _data, _model_type
    for sample in _data:
        X = np.load(sample)
        if _model_type == 'cnn' or _model_type == 'dscnn':
            X = X.reshape((1, *X.shape, NUM_AUDIO_CHANNELS))
        if _model_type == 'dnn':
            X = X.reshape((1, *X.shape))
        X = np.array(X, dtype=np.float32)
        yield [X]


def _model_size(model):
    # Calculating the actual number of floats in the model
    return sum([np.prod(tf.compat.v1.keras.backend.get_value(w).shape) for w in model.trainable_weights])


def display_device_info():
    logger.info(f"Devices:")
    _device_used = None
    devices = [tf.config.experimental.list_physical_devices(dev) for dev in ['CPU', 'GPU', 'TPU']]
    for device in devices:
        if device:
            for d in device:
                logger.info(f"{d.device_type} -> {d.name}")
    _device_used = d
    logger.info(f"Using device : {_device_used.device_type} -> {_device_used.name}")


if __name__ == '__main__':
    # Creating tf-lite and tf-lite-quantized models from the saved keras models.

    import os

    keras_models_dir = Path(MODELS_DIR / "regular/run-8")

    tflite_model_dir = Path(MODELS_DIR / "tf-lite")
    quantized_model_dir = Path(MODELS_DIR / "quantized")
    keras_models = [keras_models_dir / model_path for model_path in os.listdir(keras_models_dir)
                    if model_path.endswith(".h5")]

    # Converting keras models to tf-lite format
    for model_path in keras_models:
        model_to_tflite(model_path, tflite_model_dir)
    # Performing 8-bit quantization
    for model_path in keras_models:
        model_to_tflite(model_path, quantized_model_dir, quantize=True, quant_type="8-bit", filename_append="_8-bit")
    # Performing float-16 quantization
    for model_path in keras_models:
        model_to_tflite(model_path, quantized_model_dir, quantize=True, quant_type="float-16", filename_append="_float-16")
    # Performing full-integer quantization
    df = get_dataframe(VER, df_type="_features_clean")
    for model_path in keras_models:
        _data = df[df['category'] == 'testing'].sample(100).index
        _model_type, _ = get_model_type(model_path)
        model_to_tflite(model_path, quantized_model_dir, quantize=True, quant_type="full-int", filename_append="_full-int",
                        repr_data_gen=representative_data)

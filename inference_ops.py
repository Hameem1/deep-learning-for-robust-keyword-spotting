"""Contains helper functions for model inference operations."""

import os
import platform
import numpy as np
from pathlib import Path
from logger import logger
from shutil import copyfile
from data_ops import get_label_binarizer
from config import N_FRAMES, N_MFCC, NUM_AUDIO_CHANNELS, TRAIN_TEST_LABELS, BENCHMARK_DIR, VER

lb = get_label_binarizer()


def input_tensor(interpreter, batch=False):
    tensor_index = interpreter.get_input_details()[0]['index']
    if batch:
        return interpreter.tensor(tensor_index)()
    else:
        return interpreter.tensor(tensor_index)()[0]


def set_input(interpreter, data, batch=False):
    """Copies data to input tensor."""
    try:
        input_tensor(interpreter, batch=batch)[:, :] = data
    except Exception as e:
        logger.warning(f"Interpreter i/p shape: {interpreter.get_input_details()[0]['shape']}")
        logger.warning(f"Data shape: {data.shape}")
        logger.exception(e)
        exit(1)


def _make_batch(samples_df, model_type, dim=(N_FRAMES, N_MFCC), ignore_labels=False):
    """Constructs one batch of data."""
    # Initializing X and y
    batch_size = samples_df.shape[0]
    X = np.empty((batch_size, *dim))
    if not ignore_labels:
        y = np.empty((batch_size, len(TRAIN_TEST_LABELS)))

    # Constructing X and y
    for i, sample in enumerate(samples_df.index):
        X[i] = np.load(sample)
        if not ignore_labels:
            label = samples_df.loc[sample, 'label']
            y[i] = lb.transform([label])

    # Reshaping according to model type
    if model_type == 'dscnn' or model_type == 'cnn' or model_type == 'ds':
        X = X.reshape((*X.shape, NUM_AUDIO_CHANNELS))
    elif model_type == 'dnn':
        X = X.reshape((*X.shape,))
    else:
        logger.exception(f"Invalid model_type : '{model_type}'")

    # Converting to float32
    X = np.array(X, dtype=np.float32)
    if not ignore_labels:
        return X, y
    else:
        return X


def get_batch_accuracy(predictions, actual):
    assert (len(predictions) == len(actual)), \
        f"The length of 'predictions' ({len(predictions)}) and 'actual' ({len(actual)}) does not match!"
    batch_size = predictions.shape[0]
    mask = np.char.equal(lb.inverse_transform(predictions), lb.inverse_transform(actual))
    accuracy = (mask.sum() / batch_size) * 100
    return accuracy


def get_benchmark_data(df_type="_features_clean", n_samples=1000):
    from pre_processing import get_dataframe
    df = get_dataframe(VER, df_type=df_type)
    data_dir = BENCHMARK_DIR / 'data'
    samples = df[df['category'] == 'testing'].sample(n_samples)
    if len(os.listdir(data_dir)) == 0:
        for i, sample in enumerate(samples.index):
            copyfile(sample, data_dir / Path(sample).name.replace(".", f"_{i}."))
        logger.info(f"{n_samples} random samples from '{df_type} copied over to : {data_dir.as_posix()}'")
    else:
        logger.warning(f"The 'benchmark' data directory is not empty. Please manually clear it out first!")


def get_edge_tpu_lib():
    return {'Linux': 'libedgetpu.so.1',
            'Darwin': 'libedgetpu.1.dylib',
            'Windows': 'edgetpu.dll'}[platform.system()]

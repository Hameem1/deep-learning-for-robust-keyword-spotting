"""Implements advanced logging functionality."""

import os
import logging
from config import \
    HISTORY_LOG_FILTER, HISTORY_LOG_FILE, CURRENT_LOG_LEVEL, CURRENT_LOG_FILE, CONSOLE_LOG_LEVEL, LOGS_DIR


class MyFilter(object):
    """Restricts the file handler to log one specific log level."""

    def __init__(self, level):
        self.__level = level

    def filter(self, logRecord):
        return logRecord.levelno == self.__level


formatter = logging.Formatter(fmt='%(levelname)s | %(asctime)s | %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
current_log = logging.FileHandler(f'{LOGS_DIR}/{CURRENT_LOG_FILE}', mode='w')
current_log.setLevel(CURRENT_LOG_LEVEL)
current_log.setFormatter(formatter)
history_log = logging.FileHandler(f'{LOGS_DIR}/{HISTORY_LOG_FILE}', mode='a')
history_log.addFilter(MyFilter(logging.INFO))
history_log.setLevel(HISTORY_LOG_FILTER)
history_log.setFormatter(formatter)
console = logging.StreamHandler()
console.setLevel(CONSOLE_LOG_LEVEL)
console.setFormatter(formatter)

# Getting a new file name for every run
_base_name = "run-log"
_running_log_dir = LOGS_DIR / "run_logs"
_existing_files = [l.split('.')[0] for l in os.listdir(_running_log_dir) if l.__contains__(_base_name)]
if len(_existing_files) > 0:
    _max_run = max([int(m.split('_')[-1]) for m in _existing_files]) + 1
else:
    _max_run = 0
_new_filename = f"{_base_name}_{_max_run}.log"
if not _running_log_dir.exists():
    _running_log_dir.mkdir()
running_log = logging.FileHandler(f'{_running_log_dir}/{_new_filename}', mode='a')
running_log.setLevel(CURRENT_LOG_LEVEL)
running_log.setFormatter(formatter)

logger.addHandler(running_log)
logger.addHandler(current_log)
# logger.addHandler(history_log)
logger.addHandler(console)

logging.logThreads = 0
logging.logProcesses = 0
# Set this to False in production
logging.raiseExceptions = True
